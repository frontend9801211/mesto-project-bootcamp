const profileButton = document.getElementById("profileButton");
const editButton = document.getElementById("editButton");
const profilePopup = document.getElementById("profilePopup");
const editPopup = document.getElementById("editPopup");
const profileCloseButton = document.getElementById("profileCloseButton");
const editCloseButton = document.getElementById("editCloseButton");
const profileForm = document.getElementById("profileForm");
const cardForm = document.getElementById("editForm");
const userName = document.querySelector('.profile__header');
const userDescription = document.querySelector('.profile__description');
const profileName = document.getElementById("profileName");
const editName = document.getElementById("editName");
const editLink = document.getElementById("editLink");
const profileDescription = document.getElementById("profileDescription");


function openProfileForm() {
  profileName.value = userName.textContent;
  profileDescription.value = userDescription.textContent;
  openPopup(profilePopup);
}

function openPopup(popup) {
  popup.classList.add('popup_opened');
}

profileButton.addEventListener('click', openProfileForm); 

editButton.addEventListener('click', () => {
  openPopup(editPopup);
});

function submitProfileForm(evt) {
  evt.preventDefault();
  userName.textContent = profileName.value;
  userDescription.textContent = profileDescription.value;
  closePopup(profilePopup);
}

function submitCardForm(evt) {
  evt.preventDefault();
  addElement(editName.value, editLink.value);
  closePopup(editPopup);
  clearInputCard();
}

function clearInputProfile() {
  profileName.value = '';
  profileDescription.value = '';
}

function clearInputCard() {
  editName.value = 'Название';
  editLink.value = 'Ссылка';
}

function closePopup(popup) {
  popup.classList.remove('popup_opened');
}

profileForm.addEventListener('submit', submitProfileForm);

cardForm.addEventListener('submit', submitCardForm);

profileCloseButton.addEventListener('click', () => {
  closePopup(profilePopup);
  clearInputProfile();
});

editCloseButton.addEventListener('click', () => {
  closePopup(editPopup);
  clearInputCard();
}); 

const initialCards = [
  {
    name: 'Архыз',
    link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/arkhyz.jpg'
  },
  {
    name: 'Челябинская область',
    link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/chelyabinsk-oblast.jpg'
  },
  {
    name: 'Иваново',
    link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/ivanovo.jpg'
  },
  {
    name: 'Камчатка',
    link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/kamchatka.jpg'
  },
  {
    name: 'Холмогорский район',
    link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/kholmogorsky-rayon.jpg'
  },
  {
    name: 'Байкал',
    link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/baikal.jpg'
  }
];

const elementsContainer = document.querySelector('.elements');


function createCard(name, link) {
  const userTemplate = document.querySelector('#element').content;
  const userElement = userTemplate.querySelector('.element').cloneNode(true);
  userElement.querySelector(".element__text").textContent = name;
  userElement.querySelector(".element__open-text").textContent = name;
  userElement.querySelector(".element__image").src = link;
  userElement.querySelector(".element__image").setAttribute("alt", name);
  userElement.querySelector(".element__open-image").src = link;
  userElement.querySelector(".element__open-image").setAttribute("alt", name);
  return userElement;
}

function addElement(name, link) {
  const userElement = createCard(name, link);
  elementsContainer.prepend(userElement);

  userElement.querySelector('.element__icon').addEventListener('click', function (evt) {
    evt.target.classList.toggle('element__icon_active');
  });

  userElement.querySelector('.element__trash').addEventListener('click', function (evt) {
    evt.target.parentElement.remove();
  });

  userElement.querySelector('.element__image').addEventListener('click', function (evt) {
    evt.target.previousElementSibling.classList.toggle('element__open_active');
  });
  
  userElement.querySelector('.element__close').addEventListener('click', function (evt) {
    evt.target.parentNode.parentNode.classList.toggle('element__open_active');
  });
  
}


for (let i = 0; i < initialCards.length; i++) {
  addElement(initialCards[i].name, initialCards[i].link);
}


